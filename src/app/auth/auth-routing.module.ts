import { Routes, RouterModule } from "@angular/router";
import { LogInComponent } from "./log-in/log-in.component";
import { RegisterComponent } from "./register/register.component";

const routes: Routes = [
    { path: "", pathMatch: "full", redirectTo: "log-in" },
    {
        path: "log-in",
        component: LogInComponent,
    },
    {
        path: "register",
        component: RegisterComponent,
    },
    // {
    //     path: "**",
    //     component: NotFoundComponent,
    // },
];

export const AuthRoutingModule = RouterModule.forChild(routes);
