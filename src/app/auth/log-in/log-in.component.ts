import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService, UserService } from "src/app/shared/services";
import { switchMap, takeUntil } from "rxjs/operators";
import { UserModel } from "src/app/shared/models";
import { Subject, throwError } from "rxjs";

@Component({
    selector: "app-log-in",
    templateUrl: "./log-in.component.html",
    styleUrls: ["./log-in.component.less"],
})
export class LogInComponent implements OnInit, OnDestroy {
    validateForm: FormGroup;
    private ngUnsubscribe = new Subject();
    loginTitle = "LOG IN";
    isLoadingLogIn = false;

    constructor(private fb: FormBuilder, private authService: AuthService, private userService: UserService) {}

    ngOnInit(): void {
        this.validateForm = this.fb.group({
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]],
            remember: [true],
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    login() {
        this.loginTitle = "PROCESSING...";
        this.isLoadingLogIn = true;

        this.authService
            .login({
                username: this.validateForm.value.userName,
                password: this.validateForm.value.password,
            })
            .pipe(
                takeUntil(this.ngUnsubscribe),
                switchMap((username: string) => {
                        return this.userService.getUser(username);
                })
            )
            .subscribe(
                (user: UserModel) => {
                    this.loginTitle = "LOG IN";
                    this.isLoadingLogIn = false;
                    if (user) {
                        this.authService.eventCloseLoginModal();
                    }
                },
                error => {
                    this.isLoadingLogIn = false;
                    this.loginTitle = "LOG IN";
                }
            );
    }
}
