import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LogInComponent } from "./log-in/log-in.component";
import { RegisterComponent } from "./register/register.component";
import { SharedModule } from "../shared/shared.module";
import { AuthRoutingModule } from "./auth-routing.module";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthService } from "../shared/services";
import { TokenInterceptor } from "../shared/services/auth/token.interceptor";

@NgModule({
    declarations: [LogInComponent, RegisterComponent],
    providers: [
        AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true,
        },
    ],
    imports: [CommonModule, SharedModule, AuthRoutingModule],
    exports: [LogInComponent, RegisterComponent],
})
export class AuthModule {}
