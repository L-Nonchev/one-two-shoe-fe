import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormControl, FormBuilder, Validators, FormGroup } from "@angular/forms";
import { UserModel } from "src/app/shared/models";
import { Subject } from "rxjs";
import { takeUntil, switchMap } from "rxjs/operators";
import { UserService, AuthService } from "src/app/shared/services";

@Component({
    selector: "app-register",
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.less"],
})
export class RegisterComponent implements OnInit, OnDestroy {
    validateForm: FormGroup;
    registerBtnTitle = "REGISTER";
    isRegisterUser = false;

    private ngUnsubscribe = new Subject();
    constructor(private fb: FormBuilder, private authService: AuthService, private userService: UserService) {}

    ngOnInit(): void {
        this.validateForm = this.fb.group({
            email: [null, [Validators.email, Validators.required]], // TODO:: [this.userEmailAsyncValidator]
            password: [null, [Validators.required, , Validators.minLength(6)]],
            checkPassword: [null, [Validators.required, this.confirmationValidator]],
            userName: [null, [Validators.required, Validators.minLength(3)]], // TODO:: [this.userNameAsyncValidator]
            firstName: [null, [Validators.required]],
            lastName: [null, [Validators.required]],
            address: [null, [Validators.required]],
            phoneNumberPrefix: ["+359"],
            phoneNumber: [null, [Validators.required]],
            agree: [false],
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    register() {
        this.registerBtnTitle = "PROCESSING";
        this.isRegisterUser = true;

        const user = new UserModel();
        user.email = this.validateForm.value.email;
        user.username = this.validateForm.value.userName;
        user.password = this.validateForm.value.password;
        user.firstName = this.validateForm.value.firstName;
        user.lastName = this.validateForm.value.lastName;
        user.phone = this.validateForm.value.phoneNumber;
        user.address = this.validateForm.value.address;

        this.authService
            .register(user)
            .pipe(
                takeUntil(this.ngUnsubscribe),
                switchMap((username: string) => {
                    return this.userService.getUser(username);
                })
            )
            .subscribe((userData: UserModel) => {
                this.registerBtnTitle = "REGISTER";
                this.isRegisterUser = true;
                if (userData) {
                    this.userService.eventCloseReisterModal();
                }
            });
    }

    updateConfirmValidator(): void {
        Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
    }

    confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { required: true };
        } else if (control.value !== this.validateForm.controls.password.value) {
            return { confirm: true, error: true };
        }
        return {};
    };

    getCaptcha(e: MouseEvent): void {
        e.preventDefault();
    }

    // TODO:: real time validation
    // userNameAsyncValidator = () =>
    //     });
    // userEmailAsyncValidator = () =>
    //     });
}
