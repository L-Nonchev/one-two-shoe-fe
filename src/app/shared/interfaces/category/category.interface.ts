import { IImg } from "../img/img.interface";

export interface ICategory {
    id: number;
    value: string;
    imgDir: IImg;
}
