import { ItemModel, UserModel } from "../../models";

export interface IRevue {
    id: number;
    description: string;
    rating: number;
    user: UserModel;
    item: ItemModel;
}
