import { IImg } from "../img/img.interface";

export interface IAd {
    id: number;
    name: string;
    description: string;
    imgDir: IImg;
    title: string;
}
