export * from "./ad/ad.interface";
export * from "./img/img.interface";
export * from "./category/category.interface";
export * from "./colort/color.interface";
export * from "./size/size.interface";
export * from "./storage/storage.interface";