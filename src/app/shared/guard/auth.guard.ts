import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from "@angular/router";
import { AuthService } from "../services";

@Injectable({
    providedIn: "root",
})
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private router: Router, private authServica: AuthService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.authServica.isLoggedIn()) {
            return true;
        }

        this.router.navigate(["/store/home"]);
        return false;
    }

    canLoad(route: Route): boolean {
        if (this.authServica.isLoggedIn()) {
            return true;
        }

        this.router.navigate(["/store/home"]);
        return false;
    }
}
