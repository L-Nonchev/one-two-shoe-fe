import { Component, OnInit, Input } from "@angular/core";
import { ItemModel } from "../../models";

@Component({
    selector: "app-item-card",
    templateUrl: "./item-card.component.html",
    styleUrls: ["./item-card.component.less"],
})
export class ItemCardComponent implements OnInit {
    @Input() item: ItemModel;
    constructor() {}

    ngOnInit() {}
}
