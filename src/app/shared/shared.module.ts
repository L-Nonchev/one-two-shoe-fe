import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    NzGridModule,
    NzDividerModule,
    NzCardModule,
    NzListModule,
    NzButtonModule,
    NzPaginationModule,
    NzProgressModule,
    NzSkeletonModule,
    NzLayoutModule,
    NzIconModule,
    NzMenuModule,
    NzModalModule,
    NzStepsModule,
    NzFormModule,
    NzCheckboxModule,
    NzInputModule,
    NzDropDownModule,
    NzTableModule,
    NzSelectModule,
    NzRadioModule,
    NzSpinModule,
    NzPageHeaderModule,
    NzBreadCrumbModule,
    NzStatisticModule,
    NzAvatarModule,
    NzTagModule,
    NzMessageModule,
    NzBadgeModule,
    NzSwitchModule,
    NzCarouselModule,
    NzRateModule,
    NzEmptyModule,
    NzNotificationModule,
    NzInputNumberModule,
} from "ng-zorro-antd";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ItemCardComponent } from "./components/item-card/item-card.component";
import { ItemCardSkeletonComponent } from "./components/item-card-skeleton/item-card-skeleton.component";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations: [ItemCardComponent, ItemCardSkeletonComponent],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        ScrollingModule,
        NzGridModule,
        NzDividerModule,
        NzCardModule,
        NzGridModule,
        NzListModule,
        NzButtonModule,
        NzPaginationModule,
        NzProgressModule,
        NzSkeletonModule,
        NzLayoutModule,
        NzIconModule,
        NzMenuModule,
        NzModalModule,
        NzStepsModule,
        NzFormModule,
        FormsModule,
        NzCheckboxModule,
        NzInputModule,
        NzDropDownModule,
        NzTableModule,
        NzSelectModule,
        NzRadioModule,
        NzSpinModule,
        NzPageHeaderModule,
        NzBreadCrumbModule,
        NzStatisticModule,
        NzAvatarModule,
        NzTagModule,
        NzMessageModule,
        NzBadgeModule,
        NzSwitchModule,
        NzCarouselModule,
        NzRateModule,
        NzEmptyModule,
        NzNotificationModule,
        NzInputNumberModule,
    ],
    exports: [
        CommonModule,
        RouterModule,
        ScrollingModule,
        FormsModule,
        ReactiveFormsModule,
        NzGridModule,
        NzDividerModule,
        NzCardModule,
        NzListModule,
        NzButtonModule,
        NzPaginationModule,
        NzProgressModule,
        NzSkeletonModule,
        NzLayoutModule,
        NzIconModule,
        NzMenuModule,
        NzModalModule,
        NzStepsModule,
        NzFormModule,
        NzCheckboxModule,
        NzInputModule,
        NzDropDownModule,
        NzTableModule,
        NzSelectModule,
        NzRadioModule,
        NzSpinModule,
        NzPageHeaderModule,
        NzBreadCrumbModule,
        NzStatisticModule,
        NzAvatarModule,
        NzTagModule,
        NzMessageModule,
        NzBadgeModule,
        NzSwitchModule,
        NzCarouselModule,
        NzRateModule,
        NzEmptyModule,
        NzNotificationModule,
        ItemCardComponent,
        ItemCardSkeletonComponent,
        NzInputNumberModule,
    ],
    providers: [],
})
export class SharedModule {}
