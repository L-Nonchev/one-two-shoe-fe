import { ItemModel } from "./item.model";
import { ISize } from "../../interfaces";

export class ShoppingItemModel {
    item: ItemModel;
    size: ISize;
    quantity = 1;
}
