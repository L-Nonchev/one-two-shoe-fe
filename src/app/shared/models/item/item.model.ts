import { IColor, ISize, ICategory, IStorage } from "../../interfaces";
import { IRevue } from "../../interfaces/revue/revue.interface";

export class ItemModel {
    id: number;
    name: string;
    description: string;
    price: number;
    currency: string;
    quantity: number;
    discount: number;
    colors: IColor[];
    sizes: ISize[];
    categories: ICategory[];
    storages: IStorage[];
    date: string;
    revue: IRevue[];
    popularIndex: number;
    rateIndex: number;
}
