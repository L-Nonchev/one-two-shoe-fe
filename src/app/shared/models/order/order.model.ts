import { ItemModel } from "../item/item.model";

export class OrderModel {
    id: number;
    ordNumber: string;
    status: string;
    addrss: string;
    items: ItemModel[];
    total: string;
}
