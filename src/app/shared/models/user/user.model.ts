import { OrderModel } from "../order/order.model";

export class UserModel {
    id: number;
    username: string;
    password?: string;
    email: string;
    firstName: string;
    lastName: string;
    address: string;
    orders: OrderModel[];
    phone: string;
    roles?: string[];
    superAdmin?: boolean;
}
