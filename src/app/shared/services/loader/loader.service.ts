import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class LoaderService {
    showSpinner: BehaviorSubject<boolean> = new BehaviorSubject(false);
    showSpinner$ = this.showSpinner.asObservable();

    constructor() {}

    getShowSpinner(): Observable<boolean> {
        return this.showSpinner$;
    }
}
