import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ICategory } from "../../interfaces";
import { environment } from "src/environments/environment";
import { LoaderService } from "../loader/loader.service";
import { tap } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class CategoryService {
    constructor(private http: HttpClient, private loaderService: LoaderService) {}

    loadData() {
        const url = `${environment.apiUrl}/api/category`;
        return this.http.get<ICategory[]>(url).pipe(
            tap(() => {
                this.loaderService.showSpinner.next(false);
            })
        );
    }
}
