import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { IAd } from "../../interfaces";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AdService {
    constructor(private http: HttpClient) {}

    getAllads() {
        const url = `${environment.apiUrl}/ad`;

        return this.http.get<IAd[]>(url);
    }
}
