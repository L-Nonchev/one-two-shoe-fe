import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AdService } from "../ad.service";
import { Observable } from "rxjs";
import { IAd } from "src/app/shared/interfaces";
import { LoaderService } from "../../loader/loader.service";

@Injectable({
    providedIn: "root",
})
export class CarouselResolverService implements Resolve<Observable<IAd[]>> {
    constructor(private adService: AdService, private loaderService: LoaderService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAd[]> {
        this.loaderService.showSpinner.next(true);
        return this.adService.getAllads();
    }
}
