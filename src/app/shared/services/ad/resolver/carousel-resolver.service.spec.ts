import { TestBed } from '@angular/core/testing';

import { HomeResolverService } from './carousel-resolver.service';

describe('CarouselResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomeResolverService = TestBed.get(HomeResolverService);
    expect(service).toBeTruthy();
  });
});
