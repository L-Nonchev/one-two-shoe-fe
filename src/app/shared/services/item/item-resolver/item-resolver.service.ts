import { Injectable } from "@angular/core";
import { ItemModel } from "src/app/shared/models";
import { Observable } from "rxjs";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ItemService } from "../item.service";

@Injectable({
    providedIn: "root",
})
export class ItemResolverService implements Resolve<Observable<ItemModel>> {
    constructor(private itemService: ItemService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ItemModel> {
        const itemId: string = route.queryParams.item_id;

        return this.itemService.getSingelItemDataById(itemId);
    }
}
