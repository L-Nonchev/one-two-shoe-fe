import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ItemsResponse } from "../../types";
import { Subject, of, fromEvent } from "rxjs";
import { switchMap, retryWhen, catchError } from "rxjs/operators";
import { ItemModel } from "../../models";
import { ShoppingItemModel } from "../../models/item/shopping-item.model";
import { ShoppingCardService } from "../shopping-card/shopping-card.service";

@Injectable({
    providedIn: "root",
})
export class ItemService {
    private readonly SHOPINGBAG = "shopping-card";

    public subjectGetDataByFiltering = new Subject<HttpParams>();

    constructor(private http: HttpClient, private shoppingCardService: ShoppingCardService) {}

    eventGetDataByFilter(params: HttpParams) {
        this.subjectGetDataByFiltering.next(params);
    }

    catchEventGetDataByFilter() {
        return this.subjectGetDataByFiltering.pipe(
            switchMap((params: HttpParams) => {
                return this.getData(params).pipe(
                    catchError(error => of(error)),
                    retryWhen(() => fromEvent(window, "online"))
                );
            })
        );
    }

    getData(params: HttpParams) {
        const url = `${environment.apiUrl}/api/item`;
        return this.http.get<ItemsResponse>(url, { params });
    }

    getSingelItemDataById(itemId: string) {
        const url = `${environment.apiUrl}/api/item/${itemId}`;
        return this.http.get<ItemModel>(url);
    }

    addItemToLocalStorafe(shopingItem: ShoppingItemModel) {
        let findItem = false;
        let canSave = true;
        let items: ShoppingItemModel[] = [];

        if (localStorage.getItem(this.SHOPINGBAG)) {
            items = JSON.parse(localStorage.getItem(this.SHOPINGBAG));

            items.map((data: ShoppingItemModel) => {
                if (JSON.stringify(data.item) === JSON.stringify(shopingItem.item)) {
                    if (data.quantity >= shopingItem.item.quantity) {
                        canSave = false;
                        return;
                    }

                    data.quantity++;
                    findItem = true;
                    return;
                }
            });
        }

        if (!canSave) {
            return false;
        }

        if (!findItem) {
            items.push(shopingItem);
        }

        this.saveDataInLocalStorage(items);
        return true;
    }

    getItemsFromLocalStorage() {
        if (localStorage.getItem(this.SHOPINGBAG)) {
            return JSON.parse(localStorage.getItem(this.SHOPINGBAG));
        }
        return false;
    }

    deleteItemsFromLocalStorage(){
        if (localStorage.getItem(this.SHOPINGBAG)) {
            localStorage.removeItem(this.SHOPINGBAG);
            return true;
        }
        return false;
    }

    private saveDataInLocalStorage(items: ShoppingItemModel[]) {
        localStorage.setItem("shopping-card", JSON.stringify(items));
        this.shoppingCardService.eventUpdateShoppingBagIndex();
        return;
    }
}
