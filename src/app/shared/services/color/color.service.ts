import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IColor } from "../../interfaces";
import { environment } from "src/environments/environment";
import { LoaderService } from "../loader/loader.service";
import { tap } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class ColorService {
    constructor(private http: HttpClient, private loaderService: LoaderService) {}

    loadData() {
        this.loaderService.showSpinner.next(true);
        const url = `${environment.apiUrl}/api/color`;
        return this.http.get<IColor[]>(url).pipe(
            tap(() => {
                this.loaderService.showSpinner.next(false);
            })
        );
    }
}
