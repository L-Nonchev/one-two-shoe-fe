import { Injectable } from "@angular/core";
import { ISize } from "../../interfaces";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { LoaderService } from "../loader/loader.service";
import { tap } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class SizeService {
    constructor(private http: HttpClient, private loaderService: LoaderService) {}

    loadData() {
        const url = `${environment.apiUrl}/api/size`;
        return this.http.get<ISize[]>(url).pipe(
            tap(() => {
                this.loaderService.showSpinner.next(false);
            })
        );
    }
}
