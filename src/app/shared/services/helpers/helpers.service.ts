import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

@Injectable({
    providedIn: "root",
})
export class HelperService {
    constructor() {}

    public createHttpParams(urlParam: Array<Array<string>>) {
        let params = new HttpParams();

        urlParam.forEach(data => {
            let key, value;
            [key, value] = data;

            params = params.append(key, value);
        });

        return params;
    }
}
