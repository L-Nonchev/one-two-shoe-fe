import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { UserModel } from "../../models";
import { environment } from "src/environments/environment";
import { Subject } from "rxjs";
import { tap } from "rxjs/operators";
import { AuthService } from "../auth/auth.service";

@Injectable({
    providedIn: "root",
})
export class UserService {
    private readonly USER = "USER";
    private readonly USERNAME = "USERNAME";

    public subjectSuccessRegister = new Subject<null>();

    constructor(private http: HttpClient) {}

    eventCloseReisterModal() {
        this.subjectSuccessRegister.next();
    }

    catchEventCloseRegisterModal() {
        return this.subjectSuccessRegister.pipe();
    }

    update(userData: UserModel, userId: number) {
        const url = `${environment.apiUrl}/api/user/${userId}`;

        return this.http
            .put<UserModel>(url, { userData })
            .pipe(tap((user: UserModel) => this.doLoginUser(user)));
    }

    getUser(username: string) {
        const url = `${environment.apiUrl}/api/user/get_user/?username=${username}`;

        return this.http.get<UserModel>(url).pipe(tap((user: UserModel) => this.doLoginUser(user)));
    }

    logOut() {
        this.doLogoutUser();
    }

    getUserName() {
        return localStorage.getItem(this.USERNAME);
    }

    getUserFromLS() {
        return JSON.parse(localStorage.getItem(this.USER));
    }

    updateUserDataInLS(user: UserModel) {
        localStorage.setItem(this.USER, JSON.stringify(user));
        localStorage.setItem(this.USERNAME, user.username);
    }

    private doLoginUser(user: UserModel) {
        localStorage.setItem(this.USER, JSON.stringify(user));
        localStorage.setItem(this.USERNAME, user.username);
    }

    private doLogoutUser() {
        localStorage.removeItem(this.USER);
        localStorage.removeItem(this.USERNAME);
    }
}
