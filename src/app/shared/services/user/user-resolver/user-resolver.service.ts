import { Injectable } from "@angular/core";
import { UserModel } from "src/app/shared/models";
import { Observable } from "rxjs";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { UserService } from "../user.service";
import { AuthService } from "../../auth/auth.service";
import { LoaderService } from "../../loader/loader.service";

@Injectable({
    providedIn: "root",
})
export class UserResolverService implements Resolve<Observable<UserModel>> {
    constructor(private userService: UserService, private loaderService: LoaderService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserModel> {
        this.loaderService.showSpinner.next(true);
        const username = this.userService.getUserName();
        return this.userService.getUser(username);
    }
}
