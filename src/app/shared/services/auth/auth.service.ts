import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable, of, Subject, throwError } from "rxjs";
import { catchError, mapTo, tap } from "rxjs/operators";
import { Tokens, UserModel } from "../../models";
import { NzMessageService } from "ng-zorro-antd";
import { UserService } from "../user/user.service";

@Injectable({
    providedIn: "root",
})
export class AuthService {
    public subjectSuccessLogin = new Subject<null>();

    private readonly JWT_TOKEN = "JWT_TOKEN";
    private readonly REFRESH_TOKEN = "REFRESH_TOKEN";

    constructor(private http: HttpClient, private message: NzMessageService, private userService: UserService) {}

    eventCloseLoginModal() {
        this.subjectSuccessLogin.next();
    }

    catchEventCloseLoginModal() {
        return this.subjectSuccessLogin.pipe();
    }

    login(user: { username: string; password: string }) {
        const url = `${environment.apiUrl}/api/auth/login`;

        return this.http.post<Tokens>(url, user).pipe(
            tap(tokens => this.doLoginUser(tokens)),
            mapTo(user.username),
            catchError(error => {
                this.message.create("error", `Invalid credentials.`);
                return throwError(error);
            })
        );
    }

    register(user: UserModel) {
        const url = `${environment.apiUrl}/api/auth/register`;

        return this.http.post<Tokens>(url, user).pipe(
            tap(tokens => this.doLoginUser(tokens)),
            mapTo(user.username),
            catchError(error => {
                return of(false);
            })
        );
    }

    logout() {
        const url = `${environment.apiUrl}/api/auth/logout`;
        return this.http
            .post<any>(url, {
                refreshToken: this.getRefreshToken(),
            })
            .pipe(
                tap(() => {
                    this.doLogoutUser();
                    this.userService.logOut();
                }),
                mapTo(true),
                catchError(error => {
                    this.message.create("error", `This is a message of error`);
                    return of(false);
                })
            );
    }

    isLoggedIn() {
        return !!this.getJwtToken();
    }

    refreshToken() {
        const url = `${environment.apiUrl}/api/token/refresh`;
        return this.http
            .post<any>(url, {
                refreshToken: this.getRefreshToken(),
            })
            .pipe(
                tap((tokens: Tokens) => {
                    this.storeJwtToken(tokens.token);
                })
            );
    }

    getJwtToken() {
        return localStorage.getItem(this.JWT_TOKEN);
    }

    private doLoginUser(tokens: Tokens) {
        this.storeTokens(tokens);
    }

    private doLogoutUser() {
        this.removeTokens();
    }

    private getRefreshToken() {
        return localStorage.getItem(this.REFRESH_TOKEN);
    }

    private storeJwtToken(token: string) {
        localStorage.setItem(this.JWT_TOKEN, token);
    }

    private storeTokens(tokens: Tokens) {
        localStorage.setItem(this.JWT_TOKEN, tokens.token);
        localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
    }

    private removeTokens() {
        localStorage.removeItem(this.JWT_TOKEN);
        localStorage.removeItem(this.REFRESH_TOKEN);
    }
}
