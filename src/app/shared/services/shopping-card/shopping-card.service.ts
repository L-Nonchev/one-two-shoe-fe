import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class ShoppingCardService {
    public subjectUpdateShoppingBagIndex = new Subject<null>();

    constructor() {}

    eventUpdateShoppingBagIndex() {
        this.subjectUpdateShoppingBagIndex.next();
    }

    catchEventUpdateShoppingBagIndex() {
        return this.subjectUpdateShoppingBagIndex.pipe();
    }
}
