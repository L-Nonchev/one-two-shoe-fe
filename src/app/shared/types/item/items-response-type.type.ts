import { ItemModel } from "../../models";

export type ItemsResponse = {
    total_pages: string;
    total_items: string;
    items: ItemModel[];
};
