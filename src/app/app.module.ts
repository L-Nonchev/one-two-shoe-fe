import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NZ_I18N, en_US } from "ng-zorro-antd";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { registerLocaleData } from "@angular/common";
import en from "@angular/common/locales/en";
import { SharedModule } from "./shared/shared.module";
import { CoreModule } from "./core/core.module";
import { AdService, LoaderService } from "./shared/services";
import { AuthModule } from "./auth/auth.module";

registerLocaleData(en);

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        SharedModule,
        CoreModule,
        AuthModule,
    ],
    providers: [{ provide: NZ_I18N, useValue: en_US }, AdService, LoaderService],
    bootstrap: [AppComponent],
    exports: [AuthModule],
})
export class AppModule {}
