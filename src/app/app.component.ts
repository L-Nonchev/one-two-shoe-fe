import { Component, OnInit, OnDestroy } from "@angular/core";
import { NzIconService } from "ng-zorro-antd";
import { environment } from "src/environments/environment";
import { LoaderService } from "./shared/services";
import { Subject } from "rxjs";
import { takeUntil, delay } from "rxjs/operators";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.less"],
})
export class AppComponent implements OnInit, OnDestroy {
    title = "one-two-shoe-fe";
    isSpinning = true;

    private ngUnsubscribe = new Subject();

    constructor(private iconService: NzIconService, private loaderService: LoaderService) {
        this.iconService.fetchFromIconfont({ scriptUrl: `${environment.iconfontUrl}` });
    }

    ngOnInit() {
        this.loaderService
            .getShowSpinner()
            .pipe(delay(0), takeUntil(this.ngUnsubscribe))
            .subscribe((value: boolean) => {
                this.isSpinning = value;
            });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }
}
