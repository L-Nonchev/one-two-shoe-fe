import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./shared/guard/auth.guard";

const routes: Routes = [
    { path: "", pathMatch: "full", redirectTo: "store" },
    {
        path: "store",
        loadChildren: () => import("./store/store.module").then(m => m.StoreModule),
    },
    {
        path: "user",
        loadChildren: () => import("./user/user.module").then(m => m.UserModule),
        canLoad: [AuthGuard],
    },
    // {
    //     path: "**",
    //     component: NotFoundComponent,
    // },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
