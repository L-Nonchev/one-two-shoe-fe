import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { ProductListingComponent } from "./product-listing/product-listing.component";
import { ProductDetailsComponent } from "./product-details/product-details.component";
import { ShoppingCardComponent } from "./shopping-card/shopping-card.component";
import { CarouselResolverService } from "../shared/services/ad/resolver/carousel-resolver.service";
import { ItemResolverService } from "../shared/services/item/item-resolver/item-resolver.service";

const routes: Routes = [
    { path: "", pathMatch: "full", redirectTo: "home" },
    {
        path: "home",
        component: HomeComponent,
        resolve: { carouselResolverService: CarouselResolverService },
    },
    {
        path: "products-list",
        component: ProductListingComponent,
    },

    {
        path: "product-detail",
        component: ProductDetailsComponent,
        resolve: { itemResolverService: ItemResolverService },
    },

    {
        path: "shopping-card",
        component: ShoppingCardComponent,
    },

    // {
    //     path: "**",
    //     component: NotFoundComponent,
    // },
];

export const StoreRoutingModule = RouterModule.forChild(routes);
