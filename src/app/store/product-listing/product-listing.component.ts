import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subject, of, fromEvent, Observable } from "rxjs";
import { FilterSearchEnum } from "src/app/shared/enums";
import { ItemService, HelperService, SizeService, ColorService, CategoryService } from "src/app/shared/services";
import { catchError, retryWhen, takeUntil } from "rxjs/operators";
import { ItemsResponse } from "src/app/shared/types";
import { ItemModel } from "src/app/shared/models";
import { ISize } from "src/app/shared/interfaces";

@Component({
    selector: "app-product-listing",
    templateUrl: "./product-listing.component.html",
    styleUrls: ["./product-listing.component.less"],
})
export class ProductListingComponent implements OnInit, OnDestroy {
    private ngUnsubscribe = new Subject();

    // sort
    listOfOptionSort = [
        { name: "Newest", value: `${FilterSearchEnum.DESC}${FilterSearchEnum.Id}` },
        { name: "Name", value: `${FilterSearchEnum.ASC}${FilterSearchEnum.Name}` },
        { name: "Price: High-Low", value: `${FilterSearchEnum.DESC}${FilterSearchEnum.Price}` },
        { name: "Price: Low-High", value: `${FilterSearchEnum.ASC}${FilterSearchEnum.Price}` },
    ];
    selectedSortValue: string;

    // listing
    page = "1";
    total = "1";
    perPage = "9";
    items: ItemModel[];
    simulateSkeletonItems: string[] = ["1", "2", "3", "1", "2", "3", "1"];
    hasItemsData = true;
    isLoading = false;

    // filter
    sizes$: Observable<ISize[]> = this.sizeService.loadData();
    colors$: Observable<ISize[]> = this.colorService.loadData();
    categories$: Observable<ISize[]> = this.categoryService.loadData();
    private selectedColors: string[];
    private selectedSizes: string[];
    private selectedCategorires: string[];

    constructor(
        private itemService: ItemService,
        private helperService: HelperService,
        private sizeService: SizeService,
        private colorService: ColorService,
        private categoryService: CategoryService
    ) {
        this.getDataByFilter();
    }

    ngOnInit() {
        this.loadData();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    loadData() {
        this.isLoading = true;
        const data = this.getHttpParams();
        const params = this.helperService.createHttpParams(data);

        this.itemService.eventGetDataByFilter(params);
    }

    selectSize(data: string[]) {
        this.selectedSizes = data;
        this.loadData();
    }

    selectColor(data: string[]) {
        this.selectedColors = data;
        this.loadData();
    }

    selectCategory(data: string[]) {
        this.selectedCategorires = data;
        this.loadData();
    }

    private getHttpParams() {
        const data = [
            [`${FilterSearchEnum.Page}`, this.page],
            [`${FilterSearchEnum.PerPage}`, this.perPage],
            [`${FilterSearchEnum.Sort}[]`, `${FilterSearchEnum.DESC}${FilterSearchEnum.Id}`],
        ];

        // override sort value
        if (this.selectedSortValue) {
            data[2][1] = this.selectedSortValue;
        }

        // add filter by size
        if (this.selectedSizes && this.selectedSizes.length) {
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Size}][${FilterSearchEnum.Type}]`,
                `${FilterSearchEnum.Equal}`,
            ]);
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Size}][${FilterSearchEnum.Value}]`,
                JSON.stringify(this.selectedSizes),
            ]);
        }

        // add filter by color
        if (this.selectedColors && this.selectedColors.length) {
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Color}][${FilterSearchEnum.Type}]`,
                `${FilterSearchEnum.Equal}`,
            ]);
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Color}][${FilterSearchEnum.Value}]`,
                JSON.stringify(this.selectedColors),
            ]);
        }

        // add filter by category
        if (this.selectedCategorires && this.selectedCategorires.length) {
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Category}][${FilterSearchEnum.Type}]`,
                `${FilterSearchEnum.Equal}`,
            ]);
            data.push([
                `${FilterSearchEnum.Filter}[${FilterSearchEnum.Category}][${FilterSearchEnum.Value}]`,
                JSON.stringify(this.selectedCategorires),
            ]);
        }

        return data;
    }

    private getDataByFilter() {
        this.itemService
            .catchEventGetDataByFilter()
            .pipe(
                takeUntil(this.ngUnsubscribe),
                catchError(error => of(error)),
                retryWhen(() => fromEvent(window, "online"))
            )
            .subscribe((response: ItemsResponse) => {
                this.items = response.items;
                this.total = response.total_items;

                this.isLoading = false;
                if (!this.items.length) {
                    this.hasItemsData = false;
                }
            });
    }
}
