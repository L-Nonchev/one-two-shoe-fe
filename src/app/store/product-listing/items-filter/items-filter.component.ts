import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ISize, IColor, ICategory } from "src/app/shared/interfaces";
import { Observable } from "rxjs";

@Component({
    selector: "app-items-filter",
    templateUrl: "./items-filter.component.html",
    styleUrls: ["./items-filter.component.less"],
})
export class ItemsFilterComponent implements OnInit {
    @Input() listOfdata$: Observable<ISize[] | IColor[] | ICategory[]>;
    @Input() nzSpan: number;
    @Input() nzTitle: string;
    @Output() selectedData = new EventEmitter<string[]>();

    constructor() {}

    ngOnInit() {}

    choseValue(value: string[]) {
        this.selectedData.emit(value);
    }
}
