import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeComponent } from "./home/home.component";
import { ProductListingComponent } from "./product-listing/product-listing.component";
import { ProductDetailsComponent } from "./product-details/product-details.component";
import { ShoppingCardComponent } from "./shopping-card/shopping-card.component";
import { SharedModule } from "../shared/shared.module";
import { StoreRoutingModule } from "./store-routing.module";
import { CoreModule } from "../core/core.module";
import { CarouselComponent } from "./home/carousel/carousel.component";
import { AdService } from "../shared/services";
import { CategoryListingProductsComponent } from "./home/category-listing-products/category-listing-products.component";
import { ItemsFilterComponent } from "./product-listing/items-filter/items-filter.component";

@NgModule({
    declarations: [
        HomeComponent,
        ProductListingComponent,
        ProductDetailsComponent,
        ShoppingCardComponent,
        CarouselComponent,
        CategoryListingProductsComponent,
        ItemsFilterComponent,
    ],
    imports: [CommonModule, SharedModule, StoreRoutingModule, CoreModule],
    providers: [AdService],
})
export class StoreModule {}
