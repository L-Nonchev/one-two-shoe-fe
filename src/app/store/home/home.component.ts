import { Component, OnInit } from "@angular/core";
import { FilterSearchEnum } from "../../shared/enums";
import { LoaderService } from "src/app/shared/services";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.less"],
})
export class HomeComponent implements OnInit {
    filter = FilterSearchEnum;
    isSpinning = true;

    constructor(private loaderService: LoaderService) {}

    ngOnInit() {
        this.loaderService.showSpinner.next(false);
    }
}
