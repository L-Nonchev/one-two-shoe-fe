import { Component, OnInit, Input } from "@angular/core";
import { AdService } from "src/app/shared/services/ad/ad.service";
import { Observable, Subject } from "rxjs";
import { IAd } from "src/app/shared/interfaces";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "app-carousel",
    templateUrl: "./carousel.component.html",
    styleUrls: ["./carousel.component.less"],
})
export class CarouselComponent implements OnInit {
    carousels: IAd[];

    constructor(private adService: AdService, private activatedRoute: ActivatedRoute) {
        this.loadResolveData();
    }

    ngOnInit() {}

    private loadResolveData() {
        this.carousels = this.activatedRoute.snapshot.data.carouselResolverService;
    }
}
