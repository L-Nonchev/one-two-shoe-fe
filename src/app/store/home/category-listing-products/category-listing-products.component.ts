import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { ItemService, HelperService } from "src/app/shared/services";
import { FilterSearchEnum } from "../../../shared/enums";
import { Observable, Subject, of, fromEvent } from "rxjs";
import { ItemsResponse } from "src/app/shared/types";
import { ItemModel } from "src/app/shared/models";
import { catchError, retryWhen, takeUntil } from "rxjs/operators";

@Component({
    selector: "app-category-listing-products",
    templateUrl: "./category-listing-products.component.html",
    styleUrls: ["./category-listing-products.component.less"],
})
export class CategoryListingProductsComponent implements OnInit, OnDestroy {
    @Input() nzGutter: number;
    @Input() nzSpan: number;
    @Input() categoryTitle: string;
    @Input() filter: string;
    @Input() sort: string;

    private ngUnsubscribe = new Subject();

    items: ItemModel[];
    simulateSkeletonItems: string[] = ["1", "2", "3", "1", "2", "3", "1"];

    constructor(private itemService: ItemService, private helperService: HelperService) {}

    ngOnInit() {
        this.loadData();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    private loadData() {
        const data = [
            [`${FilterSearchEnum.Page}`, "1"],
            [`${FilterSearchEnum.PerPage}`, "4"],
            [`${FilterSearchEnum.Sort}[]`, `${this.sort}${this.filter}`],
        ];

        const params = this.helperService.createHttpParams(data);
        this.itemService
            .getData(params)
            .pipe(
                takeUntil(this.ngUnsubscribe),
                catchError(error => of(error)),
                retryWhen(() => fromEvent(window, "online"))
            )
            .subscribe((response: ItemsResponse) => {
                this.items = response.items;
            });
    }
}
