import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CategoryListingProductsComponent } from "./category-listing-products.component";

describe("CategoryListingProductsComponent", () => {
    let component: CategoryListingProductsComponent;
    let fixture: ComponentFixture<CategoryListingProductsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CategoryListingProductsComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CategoryListingProductsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
