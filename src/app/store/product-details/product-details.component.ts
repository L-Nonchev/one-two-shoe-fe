import { Component, OnInit, TemplateRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ItemService } from "src/app/shared/services";
import { NzNotificationService } from "ng-zorro-antd";
import { ShoppingItemModel } from "src/app/shared/models";

@Component({
    selector: "app-product-details",
    templateUrl: "./product-details.component.html",
    styleUrls: ["./product-details.component.less"],
})
export class ProductDetailsComponent implements OnInit {
    shopingItem: ShoppingItemModel = new ShoppingItemModel();
    disableShopBtn = false;
    isFavorite = false;

    constructor(
        private itemSerivce: ItemService,
        private activatedRoute: ActivatedRoute,
        private notification: NzNotificationService
    ) {
        this.loadResolveData();
    }

    ngOnInit() {}

    addProductToShoppingCard(template: TemplateRef<{}>) {
        let isSave: boolean;
        this.disableShopBtn = !this.disableShopBtn;
        isSave = this.itemSerivce.addItemToLocalStorafe(this.shopingItem);

        if (!isSave) {
            this.notification.create(
                "error",
                "Failed to add product to your card",
                `Sorry, you have reached the quantity limit ot this product ${this.shopingItem.item.name}`
            );
            this.disableShopBtn = !this.disableShopBtn;
            return;
        }

        this.notification.template(template, {
            nzStyle: {
                width: "480px",
                marginLeft: "-100px",
            },
        });

        // waiting for notification end
        setTimeout(() => {
            this.disableShopBtn = !this.disableShopBtn;
        }, 4500);
    }

    addProductToFavorite() {
        this.isFavorite = !this.isFavorite;
    }

    private loadResolveData() {
        this.shopingItem.item = this.activatedRoute.snapshot.data.itemResolverService;
    }
}
