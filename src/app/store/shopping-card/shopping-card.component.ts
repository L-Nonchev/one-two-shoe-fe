import { Component, OnInit } from "@angular/core";
import { ShoppingItemModel } from "src/app/shared/models";
import { ItemService, LoaderService } from "src/app/shared/services";

@Component({
    selector: "app-shopping-card",
    templateUrl: "./shopping-card.component.html",
    styleUrls: ["./shopping-card.component.less"],
})
export class ShoppingCardComponent implements OnInit {
    shopingBag: ShoppingItemModel[];

    constructor(private itemStorage: ItemService, private loaderService: LoaderService) {}

    ngOnInit() {
        this.shopingBag = this.itemStorage.getItemsFromLocalStorage();
    }
}
