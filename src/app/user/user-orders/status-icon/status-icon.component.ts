import { Component, OnInit, Input } from "@angular/core";

@Component({
    selector: "app-status-icon",
    templateUrl: "./status-icon.component.html",
    styleUrls: ["./status-icon.component.less"],
})
export class StatusIconComponent implements OnInit {
    @Input() status: string;
    constructor() {}

    ngOnInit() {}
}
