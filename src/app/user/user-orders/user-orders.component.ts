import { Component, OnInit } from "@angular/core";
import { UserService, LoaderService } from "src/app/shared/services";
import { UserModel, OrderModel } from "src/app/shared/models";

@Component({
    selector: "app-user-orders",
    templateUrl: "./user-orders.component.html",
    styleUrls: ["./user-orders.component.less"],
})
export class UserOrdersComponent implements OnInit {
    orders: OrderModel[];
    constructor(private userService: UserService, private loaderService: LoaderService) {}

    ngOnInit() {
        this.loaderService.showSpinner.next(false);
        const user: UserModel = this.userService.getUserFromLS();
        this.orders = user.orders;
    }
}
