import { Component, OnInit, Input } from "@angular/core";
import { ItemModel } from "src/app/shared/models";

@Component({
    selector: "app-list-order-items",
    templateUrl: "./list-order-items.component.html",
    styleUrls: ["./list-order-items.component.less"],
})
export class ListOrderItemsComponent implements OnInit {
    @Input() ordersItem: ItemModel[];
    constructor() {}

    ngOnInit() {}
}
