import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserComponent } from "./user.component";
import { UserRoutingModule } from "./user-routing.module";
import { SharedModule } from "../shared/shared.module";
import { UserDataComponent } from "./user-data/user-data.component";
import { UserOrdersComponent } from "./user-orders/user-orders.component";
import { ListOrderItemsComponent } from "./user-orders/list-order-items/list-order-items.component";
import { StatusIconComponent } from "./user-orders/status-icon/status-icon.component";
@NgModule({
    declarations: [UserComponent, UserDataComponent, UserOrdersComponent, ListOrderItemsComponent, StatusIconComponent],
    imports: [CommonModule, UserRoutingModule, SharedModule],
})
export class UserModule {}
