import { Component, OnInit } from "@angular/core";
import { UserModel } from "../shared/models";
import { ActivatedRoute } from "@angular/router";
@Component({
    selector: "app-user",
    templateUrl: "./user.component.html",
    styleUrls: ["./user.component.less"],
})
export class UserComponent implements OnInit {
    user: UserModel;
    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.loadResolveData();
    }

    private loadResolveData() {
        this.user = this.activatedRoute.snapshot.data.userResolverService;
    }
}
