import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Subject } from "rxjs";
import { UserService } from "src/app/shared/services";
import { UserModel } from "src/app/shared/models";
import { takeUntil } from "rxjs/operators";
import { NzMessageService } from "ng-zorro-antd";

@Component({
    selector: "app-user-data",
    templateUrl: "./user-data.component.html",
    styleUrls: ["./user-data.component.less"],
})
export class UserDataComponent implements OnInit {
    validateForm: FormGroup;
    user: UserModel;
    updateBtnTitle = "UPDATE";
    isUpateUser = false;

    private ngUnsubscribe = new Subject();

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private message: NzMessageService,
    ) {
        this.user = this.userService.getUserFromLS();
    }

    ngOnInit(): void {
        this.validateFormGroup();
        this.insertUserDataInForm();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    updateUser() {
        this.isUpateUser = true;
        this.updateBtnTitle = "PROCESSING";

        const user = new UserModel();
        user.email = this.validateForm.value.email;
        user.username = this.validateForm.value.userName;
        user.password = this.validateForm.value.password;
        user.firstName = this.validateForm.value.firstName;
        user.lastName = this.validateForm.value.lastName;
        user.phone = this.validateForm.value.phoneNumber;
        user.address = this.validateForm.value.address;

        this.userService
            .update(user, this.user.id)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((userData: UserModel) => {
                this.message.create("success", `User is updated.`);
                this.isUpateUser = false;
                this.updateBtnTitle = "UPDATE";
            });
    }

    updateConfirmValidator(): void {
        Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
    }

    private validateFormGroup() {
        this.validateForm = this.fb.group({
            email: [null, [Validators.email, Validators.required]], // TODO:: [this.userEmailAsyncValidator]
            password: [null, [Validators.nullValidator, Validators.minLength(6)]],
            checkPassword: [null, [Validators.required, this.confirmationValidator]],
            userName: [null, [Validators.required, Validators.minLength(3)]], // TODO:: [this.userNameAsyncValidator]
            firstName: [null, [Validators.required]],
            lastName: [null, [Validators.required]],
            address: [null, [Validators.required]],
            phoneNumberPrefix: ["+359"],
            phoneNumber: [null, [Validators.required]],
        });
    }

    private insertUserDataInForm() {
        if (this.user) {
            this.validateForm.patchValue({
                email: this.user.email,
                userName: this.user.username,
                firstName: this.user.firstName,
                lastName: this.user.lastName,
                phoneNumber: this.user.phone,
                address: this.user.address,
            });
        }
    }

    private confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { required: true };
        } else if (control.value !== this.validateForm.controls.password.value) {
            return { confirm: true, error: true };
        }
        return {};
    };
}
