import { Routes, RouterModule } from "@angular/router";
import { UserComponent } from "./user.component";
import { UserResolverService } from "../shared/services";
import { UserDataComponent } from "./user-data/user-data.component";
import { UserOrdersComponent } from "./user-orders/user-orders.component";

const routes: Routes = [
    { path: "", pathMatch: "full", redirectTo: "" },
    {
        path: "",
        component: UserComponent,
        resolve: { userResolverService: UserResolverService },
        children: [
            {
                path: "data",
                component: UserDataComponent,
            },
            {
                path: "orders",
                component: UserOrdersComponent,
            },
            { path: "", pathMatch: "full", redirectTo: "orders" },
        ],
    },
];

export const UserRoutingModule = RouterModule.forChild(routes);
