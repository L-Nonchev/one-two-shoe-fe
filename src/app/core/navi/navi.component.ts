import { Component, OnInit, OnDestroy } from "@angular/core";
import { ShoppingItemModel } from "src/app/shared/models";
import { ShoppingCardService, AuthService, UserService, ItemService } from "src/app/shared/services";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Router } from "@angular/router";

@Component({
    selector: "app-navi",
    templateUrl: "./navi.component.html",
    styleUrls: ["./navi.component.less"],
})
export class NaviComponent implements OnInit, OnDestroy {
    shopingCardItemCnt = 0;
    isVisibleLogIn = false;
    isVisibleRegister = false;
    isLoggedIn = false;
    isAdmin = false;

    private ngUnsubscribe = new Subject();

    constructor(
        private shoppingCardService: ShoppingCardService,
        private authService: AuthService,
        private userService: UserService,
        private router: Router,
        private itemService: ItemService
    ) {}

    ngOnInit() {
        this.calculateShopingItems();
        this.userIsLogin();
        this.registerUserSuccess();
        this.recalcualteShopingItems();
        this.isLoggedIn = this.authService.isLoggedIn();
    }

    logOut() {
        this.authService.logout().subscribe(success => {
            this.userService.logOut();
            this.isLoggedIn = this.authService.isLoggedIn();
            this.router.navigate(["/store/home"]);
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.unsubscribe();
    }

    showLogInModal(): void {
        this.isVisibleRegister = false;
        this.isVisibleLogIn = true;
    }

    showRegisterModal(): void {
        this.isVisibleLogIn = false;
        this.isVisibleRegister = true;
    }

    handleCancelLogIn(): void {
        this.isVisibleLogIn = false;
    }

    handleCancelRegister(): void {
        this.isVisibleRegister = false;
    }

    private calculateShopingItems() {
        this.shopingCardItemCnt = 0;
        const items = this.itemService.getItemsFromLocalStorage();
        if (items) {
            items.forEach((data: ShoppingItemModel) => {
                this.shopingCardItemCnt = this.shopingCardItemCnt + data.quantity;
            });
        }
    }

    private recalcualteShopingItems() {
        this.shoppingCardService
            .catchEventUpdateShoppingBagIndex()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
                this.calculateShopingItems();
            });
    }

    private userIsLogin() {
        this.authService
            .catchEventCloseLoginModal()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                () => {
                    this.handleCancelLogIn();
                    this.isLoggedIn = this.authService.isLoggedIn();
                },
                error => {
                    this.isLoggedIn = this.authService.isLoggedIn();
                }
            );
    }

    private registerUserSuccess() {
        this.userService
            .catchEventCloseRegisterModal()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                () => {
                    this.handleCancelLogIn();
                    this.isLoggedIn = this.authService.isLoggedIn();
                },
                error => {
                    this.isLoggedIn = this.authService.isLoggedIn();
                }
            );
    }
}
