import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NaviComponent } from "./navi/navi.component";
import { FooterComponent } from "./footer/footer.component";
import { SharedModule } from "../shared/shared.module";
import { AuthModule } from "../auth/auth.module";

@NgModule({
    declarations: [NaviComponent, FooterComponent],
    imports: [CommonModule, SharedModule, AuthModule],
    exports: [NaviComponent, FooterComponent],
})
export class CoreModule {}
